﻿using UnityEngine;

using com.ootii.Messages;

public class PlayerStats : MonoBehaviour
{
    #region Variables
    public int Money { get; private set; }
    public int StartingMoney = 500;
    public int Lives { get; private set; }
    public int StartingLives = 10;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register events
        MessageDispatcher.AddListener(EventStrings.ENEMY__DESTROYED, OnEnemyDestroy);
        MessageDispatcher.AddListener(EventStrings.ENEMY__REACHED_EXIT, OnEnemyReachExit);
        MessageDispatcher.AddListener(EventStrings.TURRET__BUILT, OnTurretBuilt);
        MessageDispatcher.AddListener(EventStrings.TURRET__REFUNDED, OnTurretRefund);
    }

    void Start()
    {
        Money = StartingMoney;
        Lives = StartingLives;

        // Notify listeners about the initial values for money and lives
        PlayerMoneyChangeData moneyChangeData = new PlayerMoneyChangeData(Money, 0);
        MessageDispatcher.SendMessageData(EventStrings.PLAYER__MONEY_CHANGE, moneyChangeData, -1);
        PlayerLostLivesData lostLivesData = new PlayerLostLivesData(Lives, 0, Lives);
        MessageDispatcher.SendMessageData(EventStrings.PLAYER__LOST_LIVES, lostLivesData, -1);
    }

    private void OnDestroy()
    {
        // Remove events
        MessageDispatcher.RemoveListener(EventStrings.ENEMY__DESTROYED, OnEnemyDestroy);
        MessageDispatcher.RemoveListener(EventStrings.ENEMY__REACHED_EXIT, OnEnemyReachExit);
        MessageDispatcher.RemoveListener(EventStrings.TURRET__BUILT, OnTurretBuilt);
        MessageDispatcher.RemoveListener(EventStrings.TURRET__REFUNDED, OnTurretRefund);
    }
    #endregion


    #region Handlers
    private void OnEnemyDestroy(IMessage message)
    {
        Enemy enemy = (Enemy)message.Data;

        AddMoney(enemy.Reward);
    }

    private void OnEnemyReachExit(IMessage message)
    {
        Enemy enemy = (Enemy)message.Data;

        RemoveLives(enemy.Damage);
    }

    private void OnTurretBuilt(IMessage message)
    {
        int cost = (int)message.Data;

        RemoveMoney(cost);
    }

    private void OnTurretRefund(IMessage message)
    {
        TurretBlueprint blueprint = (TurretBlueprint)message.Data;

        AddMoney(blueprint.Refund);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Add money to the player's balance
    /// </summary>
    /// <param name="money">Amount of money to add</param>
    public void AddMoney(int money)
    {
        if (money <= 0) return;

        Money += money;

        // Notify listeners about the money change
        PlayerMoneyChangeData moneyChangeData = new PlayerMoneyChangeData(Money, money);
        MessageDispatcher.SendMessageData(EventStrings.PLAYER__MONEY_CHANGE, moneyChangeData);
    }

    /// <summary>
    /// Subtract money from the player's balance
    /// </summary>
    /// <param name="money">Amount of money to remove</param>
    public void RemoveMoney(int money)
    {
        if (money <= 0) return;

        Money -= money;

        // Notify listeners about the money change
        PlayerMoneyChangeData moneyChangeData = new PlayerMoneyChangeData(Money, -money);
        MessageDispatcher.SendMessageData(EventStrings.PLAYER__MONEY_CHANGE, moneyChangeData);
    }

    /// <summary>
    /// Subtract from the player's lives
    /// </summary>
    /// <param name="lives">Amount of money to remove</param>
    public void RemoveLives(int livesLost)
    {
        if (livesLost <= 0) return;

        Lives -= livesLost;

        // Notify listeners that player lost lives
        PlayerLostLivesData livesLostData = new PlayerLostLivesData(Lives, livesLost, StartingLives);
        MessageDispatcher.SendMessageData(EventStrings.PLAYER__LOST_LIVES, livesLostData);

        // End the game when player has no more lives
        if (Lives <=0)
            GameManager.Instance.LoseGame();
    }
    #endregion
}
