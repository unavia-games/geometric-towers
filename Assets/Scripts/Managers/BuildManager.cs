﻿using UnityEngine;

using Unavia.Utilities;
using com.ootii.Messages;


public class BuildManager : GameSingleton<BuildManager>
{
    #region Variables
    [Header("Objects")]
    public PlayerStats PlayerStats;

    [Header("Effects")]
    public AudioClip TurretBuildSound;
    public AudioClip TurretBuildInvalidSound;
    public AudioClip TurretRefundSound;

    public OldTile SelectedTile { get; private set; }
    public TurretBlueprint SelectedBlueprint { get; private set; }
    public bool HasBlueprintSelected
    {
        get { return SelectedBlueprint != null; }
    }
    public bool CanBuildBlueprint
    {
        get { return HasBlueprintSelected ? PlayerStats.Money >= SelectedBlueprint.Cost : false; }
    }
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register handlers
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
    }

    private void Start()
    {
        DeselectBlueprint();
        DeselectTile();
    }

    private void OnDestroy()
    {
        // Remove handlers
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Clean up the build manager when the game ends
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        DeselectBlueprint();
        DeselectTile();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Build a turret on a tile
    /// </summary>
    /// <param name="tile">Selected tile</param>
    /// <returns>Built turret</returns>
    public GameObject BuildTurret(OldTile tile)
    {
        // Turrets can only be built while the game is running
        if (GameManager.Instance.IsGameOver) return null;

        // Cannot build without a selected turret blueprint
        if (SelectedBlueprint == null) return null;

        if (PlayerStats.Money < SelectedBlueprint.Cost)
        {
            // TODO: Update to play at valid location
            if (TurretBuildInvalidSound)
                AudioManager.Instance.PlayEffect(TurretBuildInvalidSound, Camera.main.transform.position, 0.25f);

            // Notify listeners that player attempted to build a turret
            MessageDispatcher.SendMessage(EventStrings.TURRET__NOT_ENOUGH_MONEY);
            return null;
        }

        // Notify listeners that a turret was built
        MessageDispatcher.SendMessageData(EventStrings.TURRET__BUILT, SelectedBlueprint.Cost);

        GameObject newTurretPrefab = SelectedBlueprint.Prefab;
        Turret newTurretComponent = newTurretPrefab.GetComponent<Turret>();
        GameObject newTurretObject = Instantiate(
            newTurretPrefab,
            tile.Base.transform.position + new Vector3(0, newTurretComponent.BuildOffset, 0),
            tile.Base.transform.rotation,
            tile.transform
        );

        // TODO: Update to use valid location
        if (TurretBuildSound)
            // AudioManager.Instance.PlayEffect(TurretBuildSound, Camera.main.transform.position);
            AudioManager.Instance.PlayEffect(TurretBuildSound, tile.transform.position);

        // Spawn turret build effect (if provided)
        if (SelectedBlueprint.BuildEffect)
        {
            Instantiate(
                SelectedBlueprint.BuildEffect,
                tile.Base.transform.position + Vector3.up * 0.2f,
                Quaternion.identity,
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }

        return newTurretObject;
    }

    /// <summary>
    /// Select the turret blueprint for building
    /// </summary>
    /// <param name="blueprint">Selected turret</param>
    public void SelectBlueprint(TurretBlueprint blueprint)
    {
        // Turrets can only be selected while the game is running
        if (GameManager.Instance.IsGameOver) return;

        // Selecting the same turret should clear the turret selection
        if (SelectedBlueprint == blueprint)
        {
            DeselectBlueprint();
            return;
        }

        SelectedBlueprint = blueprint;
        MessageDispatcher.SendMessageData(EventStrings.BLUEPRINT__SELECTED, blueprint);

        // Clear selected tile when a turret blueprint is selected
        DeselectTile();
    }

    /// <summary>
    /// Deselect the selected turret blueprint
    /// </summary>
    public void DeselectBlueprint()
    {
        SelectedBlueprint = null;
        MessageDispatcher.SendMessage(EventStrings.BLUEPRINT__DESELECTED);
    }

    /// <summary>
    /// Select a board tile for management
    /// </summary>
    /// <param name="tile">Selected tile</param>
    public void SelectTile(OldTile tile)
    {
        // Tiles can only be selected while the game is running
        if (GameManager.Instance.IsGameOver) return;

        SelectedTile = tile;
        MessageDispatcher.SendMessageData(EventStrings.TILE__SELECTED, tile);

        // Clear selected turret blueprint when a tile is selected
        DeselectBlueprint();
    }

    /// <summary>
    /// Deselect the selected board tile
    /// </summary>
    public void DeselectTile()
    {
        SelectedTile = null;
        MessageDispatcher.SendMessage(EventStrings.TILE__DESELECTED);
    }
    #endregion
}
