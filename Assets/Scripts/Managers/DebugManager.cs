﻿using UnityEngine;

using Sirenix.OdinInspector;
using Unavia.Utilities;


/// <summary>
/// Types of debug situations
/// </summary>
public enum GizmoDebug
{ 
    ALWAYS,
    SELECTED,
    NEVER
}


/// <summary>
/// Global debug configuration
/// </summary>
[ExecuteAlways]
public class DebugManager : GameSingleton<DebugManager>
{
    #region Variables
    /// <summary>
    /// Whether overall debug mode is enabled
    /// </summary>
    [Sirenix.OdinInspector.ReadOnly]
    public bool DebugMode = true;

    [FoldoutGroup("Map", true)]
    public GizmoDebug ShowWaypoints = GizmoDebug.ALWAYS;
    [FoldoutGroup("Map")]
    public GizmoDebug ShowWaypointLines = GizmoDebug.ALWAYS;

    [FoldoutGroup("Turrets", true)]
    public GizmoDebug ShowTurretRange = GizmoDebug.SELECTED;
    [FoldoutGroup("Turrets")]
    public GizmoDebug ShowTurretAim = GizmoDebug.SELECTED;
    #endregion
}
