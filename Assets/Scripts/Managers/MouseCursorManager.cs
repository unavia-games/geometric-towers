﻿using UnityEngine;

using Unavia.Utilities;


public enum CursorType
{ 
    POINTER,
    HAND,
    NO
}


public class MouseCursorManager : GameSingleton<MouseCursorManager>
{
    #region Variables
    public Texture2D CursorPointer;
    public Texture2D CursorHand;
    public Texture2D CursorNo;
    #endregion


    #region Unity Methods
    void Start()
    {
        // Set the starting moues cursor
        SetCursor(CursorType.POINTER);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Set the current cursor
    /// </summary>
    /// <param name="texture">Cursor texture</param>
    public void SetCursor(Texture2D texture)
    { 
        Cursor.SetCursor(texture, Vector2.zero, CursorMode.ForceSoftware);
    }

    /// <summary>
    /// Set the current cursor
    /// </summary>
    /// <param name="type">Cursor type</param>
    public void SetCursor(CursorType type)
    {
        switch (type)
        {
            case CursorType.POINTER:
                SetCursor(CursorPointer);
                break;
            case CursorType.HAND:
                SetCursor(CursorHand);
                break;
            case CursorType.NO:
                SetCursor(CursorNo);
                break;
            default:
                break;
        }
    }
    #endregion
}
