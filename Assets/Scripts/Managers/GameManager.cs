﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Unavia.Utilities;
using com.ootii.Messages;

public class GameManager : GameSingleton<GameManager>
{
    #region Variables
    public PlayerStats PlayerStats;
    public KeyCode TogglePauseMenu = KeyCode.Escape;
    public string MainMenuName = "MainMenu";

    public bool IsGamePaused { get; private set; }
    public bool IsGameOver { get; private set; }

    public int RoundsSurvived = 0;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register handlers
        MessageDispatcher.AddListener(EventStrings.WAVE__CHANGE, OnWaveChange);
    }

    void Start()
    {
        // Ensure time has been started properly
        Time.timeScale = 1f;

        IsGameOver = false;
        IsGamePaused = false;
    }

    private void OnDestroy()
    {
        // Remove handlers
        MessageDispatcher.RemoveListener(EventStrings.WAVE__CHANGE, OnWaveChange);
    }

    void Update()
    {
        // DEBUG: Simple way to end game while testing
        if (Input.GetKeyDown(KeyCode.E))
            LoseGame();

        // Handle toggling the game pause state
        if (Input.GetKeyDown(TogglePauseMenu))
            TogglePauseState();
    }
    #endregion


    #region Handlers
    private void OnWaveChange(IMessage message)
    {
        if (IsGameOver) return;

        RoundsSurvived = (int)message.Data;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Restart the current scene/level
    /// </summary>
    public void Retry()
    {
        Time.timeScale = 1f;

        LevelLoader.Instance.LoadLevel(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Return to the main menu
    /// </summary>
    public void Menu()
    {
        Time.timeScale = 1f;

        LevelLoader.Instance.LoadLevel(MainMenuName);
    }

    /// <summary>
    /// Toggle the game's paused state
    /// </summary>
    public void TogglePauseState()
    {
        IsGamePaused = !IsGamePaused;

        Time.timeScale = IsGamePaused ? 0f : 1f;

        MessageDispatcher.SendMessageData(EventStrings.GAME__PAUSE_TOGGLED, IsGamePaused);
    }

    /// <summary>
    /// End the game (loss condition)
    /// </summary>
    public void LoseGame()
    {
        if (IsGameOver) return;

        IsGameOver = true;

        Debug.Log("Player has died");

        GameOverData gameOverData = new GameOverData(false, RoundsSurvived);
        MessageDispatcher.SendMessageData(EventStrings.GAME__OVER, gameOverData);
    }

    /// <summary>
    /// End the game (win condition)
    /// </summary>
    public void WinGame()
    {
        if (IsGameOver) return;

        IsGameOver = true;

        Debug.Log("Player has won");

        GameOverData gameOverData = new GameOverData(true, RoundsSurvived);
        MessageDispatcher.SendMessageData(EventStrings.GAME__OVER, gameOverData);
    }
    #endregion
}
