﻿public static class EventStrings
{
    public const string BLUEPRINT__SELECTED = "BLUEPRINT__SELECTED";
    public const string BLUEPRINT__DESELECTED = "BLUEPRINT__DESELECTED";

    // public const string CONTROLS__BUILD_MODE_CHANGE = "CONTROLS__BUILD_MODE_CHANGE";
    public const string CONTROLS__CAMERA_MODE_CHANGE = "CONTROLS__CAMERA_MODE_CHANGE";

    public const string ENEMY__DESTROYED = "ENEMY__DESTROYED";
    public const string ENEMY__REACHED_EXIT = "ENEMY__REACHED_EXIT";

    public const string GAME__OVER = "GAME__OVER";
    public const string GAME__PAUSE_TOGGLED = "GAME__PAUSE_TOGGLED";

    public const string PLAYER__LOST_LIVES = "PLAYER__LOST_LIVES";
    public const string PLAYER__MONEY_CHANGE = "PLAYER__MONEY_CHANGE";

    public const string TILE__SELECTED = "TILE__SELECTED";
    public const string TILE__DESELECTED = "TILE__DESELECTED";

    public const string TURRET__BUILT = "TURRET__BUILT";
    public const string TURRET__NOT_ENOUGH_MONEY = "TURRET__NOT_ENOUGH_MONEY";
    public const string TURRET__REFUNDED = "TURRET__REFUNDED";

    public const string WAVE__APPROACHING = "WAVE__APPROACHING";
    public const string WAVE__CHANGE = "WAVE__CHANGE";
}
