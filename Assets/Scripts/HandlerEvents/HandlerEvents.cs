﻿public class GameOverData
{
    public int RoundsSurvived;
    public bool Won;

    public GameOverData(bool won, int roundsSurvived)
    {
        Won = won;
        RoundsSurvived = roundsSurvived;
    }
}

public class PlayerLostLivesData
{
    public int LivesLeft;
    public int LivesLost;
    public int LivesStart;

    public PlayerLostLivesData(int livesLeft, int livesLost, int livesStart)
    {
        LivesLeft = livesLeft;
        LivesLost = livesLost;
        LivesStart = livesStart;
    }
}

public class PlayerMoneyChangeData
{
    public int MoneyBalance;
    public int MoneyChange;

    public PlayerMoneyChangeData(int moneyBalance, int moneyChange)
    {
        MoneyBalance = moneyBalance;
        MoneyChange = moneyChange;
    }
}
