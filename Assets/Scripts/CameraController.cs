﻿using UnityEngine;

using com.ootii.Messages;

public class CameraController : MonoBehaviour
{
    #region Variables
    [Header("Panning")]
    [Range(1f, 10f)]
    public float MinPanSpeed = 2f;
    [Range(1f, 10)]
    public float MaxPanSpeed = 5f;
    [Range(0f, 20f)]
    public float MaxPanSpeedTime = 10f;
    [Range(10, 50)]
    public int PanBorder = 10;

    [Header("Zoom")]
    [Range(5, 10)]
    public int MinZoomHeight = 5;
    [Range(5, 15)]
    public int MaxZoomHeight = 10;
    [Range(1f, 10f)]
    public float ZoomSpeed = 5f;

    [Header("Miscellaneous")]
    [Range(0.25f, 5f)]
    public float CameraResetSpeed = 0.8f;
    public KeyCode ToggleMovementKey = KeyCode.Space;

    [SerializeField]
    private bool canMove = false;
    private Vector3 originalPosition;
    private float positionResetPercentage;
    // private new Camera camera;

    // Track current pan speed and how long it has been panning
    private float currentPanSpeed = 0f;
    private float currentPanTime = 0f;

    private bool isGameRunning = true;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        // Register handlers
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
    }

    private void Start()
    {
        // camera = GetComponent<Camera>();
        originalPosition = transform.position;

        currentPanSpeed = MinPanSpeed;

        UpdateMovementMode(false);
    }

    private void OnDestroy()
    {
        // Remove handlers
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
    }

    void Update()
    {
        if (!isGameRunning)
        {
            // Move the camera back to its original location
            positionResetPercentage += CameraResetSpeed * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, originalPosition, positionResetPercentage);

            return;
        }

        // Allow disabling movement
        if (Input.GetKeyDown(ToggleMovementKey))
        {
            UpdateMovementMode(!canMove);
        }

        if (canMove)
        {
            HandleMovement();
        }
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Clean up after the end of the game
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        isGameRunning = false;

        UpdateMovementMode(false);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Handle map movement (keys and mouse)
    /// </summary>
    /// <remarks>Adapted from: https://forum.unity.com/threads/rts-camera-script.72045</remarks>
    private void HandleMovement()
    {
        if (!isGameRunning) return;

        // Gather inputs
        bool isMovingUp = Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - PanBorder;
        bool isMovingDown = Input.GetKey(KeyCode.S) || Input.mousePosition.y <= PanBorder;
        bool isMovingRight = Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - PanBorder;
        bool isMovingLeft = Input.GetKey(KeyCode.A) || Input.mousePosition.x <= PanBorder;

        Vector3 panTranslation = Vector3.zero;

        if (isMovingUp && !isMovingDown)
        {
            panTranslation += Vector3.forward * currentPanSpeed * Time.deltaTime;
        }
        else if (isMovingDown && !isMovingUp)
        {
            panTranslation += Vector3.back * currentPanSpeed * Time.deltaTime;
        }

        if (isMovingRight && !isMovingLeft)
        {
            panTranslation += Vector3.right * currentPanSpeed * Time.deltaTime;
        }
        else if (isMovingLeft && !isMovingRight)
        {
            panTranslation += Vector3.left * currentPanSpeed * Time.deltaTime;
        }

        transform.Translate(panTranslation, Space.World);

        // Pan speed increases over panning time
        if (isMovingUp || isMovingDown || isMovingRight || isMovingLeft)
        {
            currentPanTime += Time.deltaTime / MaxPanSpeedTime;
            currentPanSpeed = Mathf.Lerp(MinPanSpeed, MaxPanSpeed, currentPanTime * currentPanTime);
        }
        else
        {
            currentPanTime = 0f;
            currentPanSpeed = MinPanSpeed;
        }

        // NOTE: Old approach to handle "zooming"
        // camera.fieldOfView -= Input.mouseScrollDelta.y * ZoomSpeed;

        Vector3 cameraTranslation = Vector3.zero;

        float zoomScroll = Input.mouseScrollDelta.y;

        if (Mathf.Abs(zoomScroll) > Mathf.Epsilon)
        {
            if (zoomScroll > 0 && transform.position.y > MinZoomHeight)
            {
                cameraTranslation = zoomScroll * Vector3.forward * Time.deltaTime * ZoomSpeed;
                transform.Translate(cameraTranslation, Space.Self);
            }
            else if (zoomScroll < 0 && transform.position.y < MaxZoomHeight)
            { 
                cameraTranslation = zoomScroll * Vector3.forward * Time.deltaTime * ZoomSpeed;
                transform.Translate(cameraTranslation, Space.Self);
            }
        }

        // TODO: Support changing camera rotation as camera nears the ground
        /* CurrentZoom -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 1000 * ZoomZpeed;
        CurrentZoom = Mathf.Clamp(CurrentZoom, ZoomRange.x, ZoomRange.y);
        transform.position.y -= (transform.position.y - (InitPos.y + CurrentZoom)) * 0.1;
        transform.eulerAngles.x -= (transform.eulerAngles.x - (InitRotation.x + CurrentZoom * ZoomRotation)) * 0.1; */
    }

    /// <summary>
    /// Update whether the camera can move (and notify UI)
    /// </summary>
    /// <param name="canMove">Whether movement is enabled</param>
    private void UpdateMovementMode(bool _canMove)
    {
        canMove = _canMove;

        // Notify listeners that movement mode has been disabled
        MessageDispatcher.SendMessageData(EventStrings.CONTROLS__CAMERA_MODE_CHANGE, canMove);
    }
    #endregion
}

