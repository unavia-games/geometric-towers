﻿using System;
using UnityEngine;


[Serializable]
public class Wave
{
    #region Variables
    public GameObject EnemyPrefab;
    [Range(1, 100)]
    public float EnemyCount;
    [Range(0.1f, 2f)]
    public float SpawnRate = 0.5f;
    #endregion
}
