﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

using com.ootii.Messages;
using Sirenix.OdinInspector;

public class ShopButton : MonoBehaviour
{
    #region Variables
    [Header("Objects")]
    public Button Button;
    [ReadOnly]
    public TurretBlueprint Blueprint;

    [Header("UI")]
    public Image BackgroundImage;
    public CanvasGroup DisabledCanvas;
    public Color BackgroundColor;
    public Image BorderImage;
    public Color BorderColor;
    public Image TurretIcon;
    public TextMeshProUGUI CostText;

    private Color backgroundColorClear;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register event listeners
        MessageDispatcher.AddListener(EventStrings.BLUEPRINT__SELECTED, OnBlueprintSelected);
        MessageDispatcher.AddListener(EventStrings.BLUEPRINT__DESELECTED, OnBlueprintDeselected);
        MessageDispatcher.AddListener(EventStrings.PLAYER__MONEY_CHANGE, OnPlayerMoneyChange);
    }

    private void Start()
    {
        backgroundColorClear = BackgroundColor;
        backgroundColorClear.a = 0f;
    }

    private void OnDestroy()
    {
        // Cleanup shop button listeners
        Button.onClick.RemoveAllListeners();

        // Remove event listeners
        MessageDispatcher.RemoveListener(EventStrings.BLUEPRINT__SELECTED, OnBlueprintSelected);
        MessageDispatcher.RemoveListener(EventStrings.BLUEPRINT__DESELECTED, OnBlueprintDeselected);
        MessageDispatcher.RemoveListener(EventStrings.PLAYER__MONEY_CHANGE, OnPlayerMoneyChange);
    }
    #endregion


    #region Handlers
    private void OnBlueprintSelected(IMessage message)
    {
        if (!BorderImage) return;

        TurretBlueprint selectedBlueprint = (TurretBlueprint)message.Data;

        ToggleSelected(selectedBlueprint == Blueprint);
    }

    private void OnBlueprintDeselected(IMessage message)
    {
        ToggleSelected(false);
    }

    private void OnPlayerMoneyChange(IMessage message)
    {
        PlayerMoneyChangeData moneyChange = (PlayerMoneyChangeData)message.Data;
        float balance = moneyChange.MoneyBalance;

        if (!DisabledCanvas) return;

        // TODO: Figure out to make this work with the animator
        DisabledCanvas.alpha = balance >= Blueprint.Cost ? 1 : 0.5f;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Configure the shop button
    /// </summary>
    /// <param name="blueprint">Turret blueprint</param>
    public void Set(TurretBlueprint blueprint)
    {
        Blueprint = blueprint;

        gameObject.name = string.Format("{0} Button", blueprint.Name);
        TurretIcon.sprite = blueprint.ShopIcon;
        BorderImage.color = BorderColor;
        BackgroundImage.color = backgroundColorClear;
        CostText.text = blueprint.Cost.ToString();

        Button.onClick.AddListener(OnButtonClick);
    }

    private void ToggleSelected(bool isSelected)
    {
        if (!BorderImage || !BackgroundImage) return;

        BorderImage.enabled = isSelected;
        BackgroundImage.color = isSelected ? BackgroundColor : backgroundColorClear;

        // TODO: Play animation
    }

    /// <summary>
    /// Turret shop button click handler
    /// </summary>
    private void OnButtonClick()
    {
        BuildManager.Instance.SelectBlueprint(Blueprint);
    }
    #endregion
}
