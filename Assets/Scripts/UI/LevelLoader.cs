﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using Unavia.Utilities;


/// <summary>
/// Level loader (with transitions)
/// </summary>
public class LevelLoader : GameSingleton<LevelLoader>
{
    #region Variables
    public bool DebugMode = false;
    public float TransitionTime = 1f;
    [PropertySpace]
    [Title("Available Transitions")]
    [TableList(AlwaysExpanded = true, HideToolbar = true, ShowIndexLabels = false)]
    public List<LevelLoaderItem> LevelTransitions = new List<LevelLoaderItem>();

    [Title("Computed")]
    [Sirenix.OdinInspector.ReadOnly]
    public GameObject CurrentTransition;
    [Sirenix.OdinInspector.ReadOnly]
    [ShowInInspector]
    public Animator CurrentAnimator
    {
        get
        {
            return CurrentTransition ? CurrentTransition.GetComponent<Animator>() : null;
        }
    }

    [PropertySpace]
    [Button("Get Transitions", ButtonSizes.Medium)]
    public void GetTransitionsUI()
    {
        GetTransitions();
    }
    #endregion


    #region Unity Methods
    private void Start()
    {
        // NOTE: Animation automatically plays when starting a level

        if (LevelTransitions.Count <= 0) return;

        GetTransitions();
    }

    private void Update()
    {
        if (!DebugMode) return;

        // DEBUG: Allow testing level loading easily
        if (!Input.GetKeyDown(KeyCode.Return)) return;

        LoadLevel(SceneManager.GetActiveScene().name == "TestCube" ? "TestSphere" : "TestCube");
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Fade out the level
    /// </summary>
    /// <param name="callback">Optional callback</param>
    public void FadeOut(Action callback)
    { 
        if (!CurrentAnimator)
        {
            callback?.Invoke();
            return;
        }

        CurrentAnimator.SetTrigger("FadeOut");

        Wait(TransitionTime, () =>
        {
            callback?.Invoke();
        });
    }

    /// <summary>
    /// Load a level with an animated transition
    /// </summary>
    /// <param name="name">Level name</param>
    public void LoadLevel(string name)
    {
        FadeOut(() => {
            SceneManager.LoadScene(name);
        });
    }

    /// <summary>
    /// Get the list of available transition objects
    /// </summary>
    private void GetTransitions()
    { 
        LevelTransitions.Clear();

        if (transform.childCount <= 0) return;

        foreach (Transform child in transform)
        {
            GameObject newObject = child.gameObject;
            LevelLoaderItem newItem = new LevelLoaderItem
            {
                Transition = newObject,
                Callback = () => SetCurrentTransition(newObject)
            };
            LevelTransitions.Add(newItem);
        }

        if (!CurrentTransition)
          SetCurrentTransition(LevelTransitions[0].Transition);
    }

    /// <summary>
    /// Set the selected game transition
    /// </summary>
    /// <param name="transitionObject">Transition game object</param>
    private void SetCurrentTransition(GameObject transitionObject)
    {
        // Only the current transition should be available in the hierarchy
        LevelTransitions.ForEach(t => {
            t.Transition.SetActive(t.Transition == transitionObject);
        });

        CurrentTransition = transitionObject;
    }
    #endregion
}


/// <summary>
/// Custom inspector class
/// </summary>
[Serializable]
public class LevelLoaderItem
{
    [Sirenix.OdinInspector.ReadOnly]
    public GameObject Transition;

    [TableColumnWidth(50, Resizable = false)]
    [Button("Set"), GUIColor(0.65f, 0.83f, 0.83f)]
    public void Set()
    {
        Callback?.Invoke();
    }

    [HideInInspector]
    public Action Callback;
}
