﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Unavia.Utilities;


public class SceneFader : GameSingleton<SceneFader>
{
    #region Variables
    [Range(0f, 2f)]
    public float FadeTime = 1f;
    public Image FadeImage;
    public AnimationCurve FadeCurve;
    #endregion


    #region Unity Methods
    private void Start()
    {
        StartCoroutine(FadeInRoutine());
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Fade to a specific scene
    /// </summary>
    /// <param name="scene">Scene file name</param>
    public void FadeToScene(string scene)
    {
        // Load a scene after fading out
        StartCoroutine(FadeOutRoutine(() =>
        {
            SceneManager.LoadScene(scene);
        }));
    }

    /// <summary>
    /// Fade in from the scene
    /// </summary>
    /// <param name="callback">Optional callback after fade finishes</param>
    public void FadeIn(Action callback)
    {
        StartCoroutine(FadeInRoutine(() =>
        {
            callback?.Invoke();
        }));
    }

    /// <summary>
    /// Fade out from the scene
    /// </summary>
    /// <param name="callback">Optional callback after fade finishes</param>
    public void FadeOut(Action callback)
    {
        StartCoroutine(FadeOutRoutine(() =>
        {
            callback?.Invoke();
        }));
    }

    /// <summary>
    /// Fade the screen in
    /// </summary>
    /// <param name="callback">Optional callback after fade finishes</param>
    private IEnumerator FadeInRoutine(Action callback = null)
    {
        // Ensure fade image is present and enabled
        if (!FadeImage) yield break;
        FadeImage.enabled = true;

        float time = FadeTime;

        while (time > 0f)
        {
            time -= Time.deltaTime;
            float alpha = FadeCurve.Evaluate(time);
            FadeImage.color = new Color(0f, 0f, 0f, alpha);
            yield return 0;
        }

        // Invoke optional callback
        callback?.Invoke();
    }

    /// <summary>
    /// Fade the screen out
    /// </summary>
    /// <param name="callback">Optional callback after fade finishes</param>
    private IEnumerator FadeOutRoutine(Action callback = null)
    {
        // Ensure fade image is present and enabled
        if (!FadeImage) yield break;
        FadeImage.enabled = true;

        float time = 0f;

        while (time < FadeTime)
        {
            time += Time.deltaTime;
            float alpha = FadeCurve.Evaluate(time);
            FadeImage.color = new Color(0f, 0f, 0f, alpha);
            yield return 0;
        }

        // Invoke optional callback
        callback?.Invoke();
    }
    #endregion
}
