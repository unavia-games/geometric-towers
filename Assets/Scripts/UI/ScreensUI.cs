﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

using com.ootii.Messages;
using Unavia.Utilities;

public class ScreensUI : ExtendedMonoBehaviour
{
    #region Variables
    [Header("Game Over")]
    public GameObject GameOverScreen;
    public TextMeshProUGUI RoundsSurvivedText;

    [Header("Pause Menu")]
    public GameObject PauseScreen;
    #endregion


    #region Unity Events
    private void Awake()
    {
        // Register event handlers
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.AddListener(EventStrings.GAME__PAUSE_TOGGLED, OnGamePauseToggle);
    }

    private void Start()
    {
        // Reset appropriate UI
        if (GameOverScreen)
            GameOverScreen.SetActive(false);

        if (PauseScreen)
            PauseScreen.SetActive(false);
    }

    private void OnDestroy()
    {
        // Remove event handlers
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.RemoveListener(EventStrings.GAME__PAUSE_TOGGLED, OnGamePauseToggle);
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Cleanup the UI when the game ends
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        GameOverData gameOverData = (GameOverData)message.Data;

        // TODO: Separate win and loss conditions

        if (GameOverScreen)
            GameOverScreen.SetActive(true);

        if (RoundsSurvivedText)
            RoundsSurvivedText.text = gameOverData.RoundsSurvived.ToString();
    }

    /// <summary>
    /// Toggle the game pause UI
    /// </summary>
    /// <param name="message">Whether the game is paused</param>
    private void OnGamePauseToggle(IMessage message)
    {
        bool isPaused = (bool)message.Data;

        if (PauseScreen)
          PauseScreen.SetActive(isPaused);
    }
    #endregion
}
