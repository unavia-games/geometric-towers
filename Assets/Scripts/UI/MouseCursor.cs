﻿using UnityEngine;
using UnityEngine.EventSystems;


public class MouseCursor : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Variables
    public CursorType Cursor;

    private MouseCursorManager manager;
    #endregion


    #region Unity Methods
    private void Start()
    {
        manager = MouseCursorManager.Instance;
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        manager.SetCursor(Cursor);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        manager.SetCursor(CursorType.POINTER);
    }
    #endregion
}
