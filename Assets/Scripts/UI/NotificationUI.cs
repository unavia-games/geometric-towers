﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

using Unavia.Utilities;

public class NotificationUI : GameSingleton<NotificationUI>
{
    #region Variables
    [Header("Configuration")]
    public bool AllowMultiple = false;
    // TODO: Update to support indefinite time...
    [Range(1f, 5f)]
    public float DefaultOpenTime = 2f;

    [Header("UI")]
    public GameObject NotificationBar;
    public TextMeshProUGUI NotificationText;

    // private List<string> Notifications = new List<string>();
    public string Notification { get; private set; }
    #endregion


    #region Unity Methods
    private void Start()
    {
        if (NotificationBar)
            NotificationBar.SetActive(false);
    }
    #endregion


    #region Custom Methods
    public void Notify(string notification)
    { 
        if (!AllowMultiple)
        { 
            // Hide all previous notifications (should only be one...)
        }

        Notification = notification;


        if (NotificationBar)
          NotificationBar.SetActive(true);
        if (NotificationText)
          NotificationText.text = Notification;

        // Close the notification after some time has passed
        Wait(DefaultOpenTime, () => {
            Hide();
        });
    }

    public void Hide()
    {
        if (NotificationBar)
          NotificationBar.SetActive(false);

        if (NotificationText)
          NotificationText.text = "";

        Notification = null;
    }
    #endregion
}
