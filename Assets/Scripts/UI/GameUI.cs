﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using com.ootii.Messages;
using Unavia.Utilities;

public class GameUI : ExtendedMonoBehaviour
{
    #region Variables
    [Header("Player")]
    public TextMeshProUGUI MoneyText;
    public Animator MoneyBlinkAnimator;
    [Header("Wave")]
    public TextMeshProUGUI WaveCounterText;
    [Header("Game Timer")]
    public TextMeshProUGUI GameTimerText;
    public Animator TimerBlinkAnimator;
    [Header("Miscellaneous")]
    public Image BuildIndicator;
    public Image MoveIndicator;

    private float gameTimer = 0f;
    #endregion


    #region Unity Events
    private void Awake()
    {
        // Register event handlers
        // NOTE: Some listeners must be added immediately to catch events called in "Start"...
        MessageDispatcher.AddListener(EventStrings.BLUEPRINT__SELECTED, OnBlueprintSelect);
        MessageDispatcher.AddListener(EventStrings.BLUEPRINT__DESELECTED, OnBlueprintDeselect, true);
        MessageDispatcher.AddListener(EventStrings.CONTROLS__CAMERA_MODE_CHANGE, OnCameraModeChange, true);
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.AddListener(EventStrings.PLAYER__MONEY_CHANGE, OnPlayerMoneyChange);
        MessageDispatcher.AddListener(EventStrings.TURRET__NOT_ENOUGH_MONEY, OnTurretNotEnoughMoney);
        MessageDispatcher.AddListener(EventStrings.WAVE__CHANGE, OnWaveChange);
    }

    private void Start()
    {
        // Reset appropriate UI
        if (WaveCounterText)
            WaveCounterText.text = "";
    }

    private void OnDestroy()
    {
        // Remove event handlers
        MessageDispatcher.RemoveListener(EventStrings.BLUEPRINT__SELECTED, OnBlueprintSelect);
        MessageDispatcher.RemoveListener(EventStrings.BLUEPRINT__DESELECTED, OnBlueprintDeselect);
        MessageDispatcher.RemoveListener(EventStrings.CONTROLS__CAMERA_MODE_CHANGE, OnCameraModeChange);
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.RemoveListener(EventStrings.PLAYER__MONEY_CHANGE, OnPlayerMoneyChange);
        MessageDispatcher.RemoveListener(EventStrings.TURRET__NOT_ENOUGH_MONEY, OnTurretNotEnoughMoney);
        MessageDispatcher.RemoveListener(EventStrings.WAVE__CHANGE, OnWaveChange);
    }

    private void Update()
    {
        // Timer should stop when game ends
        if (GameManager.Instance.IsGameOver) return;

        if (GameTimerText)
        {
            gameTimer += Time.deltaTime;
            TimeSpan time = TimeSpan.FromSeconds(gameTimer);
            GameTimerText.text = time.ToString(@"mm\:ss");
        }
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Cleanup the UI when the game ends
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        TimerBlinkAnimator.SetTrigger("ToggleBlinking");
    }

    /// <summary>
    /// Update the camera movement indicator
    /// </summary>
    /// <param name="message">Camera mode status</param>
    private void OnCameraModeChange(IMessage message)
    {
        if (!MoveIndicator) return;

        Color faintWhite = Color.white;
        faintWhite.a = 0.25f;

        bool canMove = (bool)message.Data;
        MoveIndicator.color = canMove ? Color.white : faintWhite;
    }

    private void OnBlueprintSelect(IMessage message)
    {
        UpdateBuildMode(true);
    }

    private void OnBlueprintDeselect(IMessage message)
    {
        UpdateBuildMode(false);
    }

    /// <summary>
    /// Update the player money UI
    /// </summary>
    /// <param name="message">Money change data</param>
    private void OnPlayerMoneyChange(IMessage message)
    {
        if (!MoneyText) return;

        PlayerMoneyChangeData moneyChangeData = (PlayerMoneyChangeData)message.Data;

        MoneyText.text = string.Format("${0}", moneyChangeData.MoneyBalance);
    }

    /// <summary>
    /// Flash the money to indicate insufficient funds
    /// </summary>
    /// <param name="message">UNUSED</param>
    private void OnTurretNotEnoughMoney(IMessage message)
    {
        if (!MoneyBlinkAnimator) return;

        MoneyBlinkAnimator.SetTrigger("Blink");
    }

    /// <summary>
    /// Update the wave counter display
    /// </summary>
    /// <param name="message">Wave counter information</param>
    private void OnWaveChange(IMessage message)
    {
        if (GameManager.Instance.IsGameOver) return;
        if (!WaveCounterText) return;

        int currentRound = (int)message.Data;

        WaveCounterText.text = string.Format("Wave {0}", currentRound);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Update the build indicator
    /// </summary>
    /// <param name="canBuild">Whether building is possible</param>
    private void UpdateBuildMode(bool canBuild)
    {
        if (!BuildIndicator) return;

        Color faintWhite = Color.white;
        faintWhite.a = 0.25f;

        BuildIndicator.color = canBuild ? Color.white : faintWhite;
    }
    #endregion
}
