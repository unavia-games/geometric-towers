﻿using UnityEngine;


public class MainMenuUI : MonoBehaviour
{
    #region Variables
    public string MainLevelName = "MainLevel";
    #endregion


    #region Custom Methods
    /// <summary>
    /// Play the game
    /// </summary>
    public void Play()
    {
        LevelLoader.Instance.LoadLevel(MainLevelName);
    }

    /// <summary>
    /// Quit the game
    /// </summary>
    public void Quit()
    {
        LevelLoader.Instance.FadeOut(() =>
        {
            Application.Quit();

            // NOTE: Quitting doesn't do anything in play mode
            Debug.Log("Quitting game");
        });
    }
    #endregion
}
