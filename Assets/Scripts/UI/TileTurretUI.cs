﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using com.ootii.Messages;
using Unavia.Utilities;

public class TileTurretUI : ExtendedMonoBehaviour
{
    #region Variables
    [Header("UI")]
    public GameObject TileUI;
    public TextMeshProUGUI RefundText;

    private OldTile tile;
    #endregion


    #region Unity Events
    private void Awake()
    {
        // Register event handlers
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.AddListener(EventStrings.TILE__SELECTED, OnTileSelect);
        MessageDispatcher.AddListener(EventStrings.TILE__DESELECTED, OnTileDeselect);
    }

    private void Start()
    {
        Hide();
    }

    private void OnDestroy()
    {
        // Remove event handlers
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.RemoveListener(EventStrings.TILE__SELECTED, OnTileSelect);
        MessageDispatcher.RemoveListener(EventStrings.TILE__DESELECTED, OnTileDeselect);
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Cleanup the UI when the game ends
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        Hide();
    }

    /// <summary>
    /// Update the selected tile UI
    /// </summary>
    /// <param name="message">Selected tile data</param>
    private void OnTileSelect(IMessage message)
    {
        OldTile tile = (OldTile)message.Data;

        Show(tile);
    }

    /// <summary>
    /// Hide the selected tile UI
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnTileDeselect(IMessage message)
    {
        Hide();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Hide the UI
    /// </summary>
    private void Hide()
    {
        tile = null;

        if (!TileUI) return;

        TileUI.SetActive(false);

        if (RefundText)
            RefundText.text = "--";
    }

    /// <summary>
    /// Show the UI at a position
    /// </summary>
    /// <param name="tile">Selected tile</param>
    private void Show(OldTile selectedTile)
    {
        tile = selectedTile;

        TileUI.SetActive(true);
        TileUI.transform.position = tile.transform.position + (Vector3.up);

        if (RefundText)
            RefundText.text = string.Format("${0}", tile.TurretBlueprint.Refund);
    }

    /// <summary>
    /// Refund the selected tile's turret
    /// </summary>
    public void RefundTurret()
    {
        if (!tile) return;

        tile.RefundTurret();
    }
    #endregion
}
