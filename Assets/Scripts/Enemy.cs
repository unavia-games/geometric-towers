﻿using UnityEngine;
using UnityEngine.UI;

using com.ootii.Messages;
using Unavia.Utilities;


public class Enemy : MonoBehaviour
{
    #region Variables
    [Header("Charateristics")]
    [Range(1f, 10f)]
    public float Speed = 2f;
    [Range(1, 100)]
    public int Reward = 1;
    [Range(1, 50)]
    public int Damage = 1;
    [Range(1f, 100f)]
    public float StartHealth = 10f;

    [Header("UI")]
    public Slider HealthBar;

    [Header("Effects")]
    public GameObject DeathEffect;
    public AudioClip HitSound;

    public float Health { get; private set; }

    private Transform target;
    private int wavepointIndex = 0;
    private float currentSpeed;
    #endregion


    #region Unity Methods
    private void Start()
    {
        target = Waypoints.Points[wavepointIndex];
        currentSpeed = Speed;
        Health = StartHealth;

        if (HealthBar)
            HealthBar.value = Health / StartHealth;
    }

    private void Update()
    {
        Vector3 direction = target.position - transform.position;
        transform.Translate(
            direction.normalized * currentSpeed * Time.deltaTime,
            Space.World
        );

        // TODO: Fix...
        // Reset speed (from laser)
        currentSpeed = Speed;

        // Transition between waypoints as necessary
        if (Vector3.Distance(transform.position, target.position) < 0.01f)
        {
            GetNextWaypoint();
        }
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Set the next waypoint for the enemy
    /// </summary>
    private void GetNextWaypoint()
    {
        // Destroy the enemy after reaching the last waypoint
        if (wavepointIndex >= Waypoints.Points.Length - 1)
        {
            Destroy(gameObject);
            return;
        }

        target = Waypoints.Points[++wavepointIndex];
    }

    /// <summary>
    /// Damage the enemy
    /// </summary>
    /// <param name="damage">Damage dealt enemy</param>
    public void TakeDamage(float damage)
    {
        Health -= damage;

        if (HealthBar)
            HealthBar.value = Health / StartHealth;

        if (HitSound)
            AudioManager.Instance.PlayEffect(HitSound, transform.position, 0.25f);

        if (Health <= Mathf.Epsilon)
        {
            Kill();
        }
    }

    /// <summary>
    /// Slow the enemy down (in the frame)
    /// </summary>
    /// <remarks>TODO: Need to refactor this...</remarks>
    /// <param name="slowdownPercentage">Percentage to slow enemy down</param>
    public void SlowDown(float slowdownPercentage)
    {
        currentSpeed = Speed * (1 - slowdownPercentage);
    }

    /// <summary>
    /// Kill the enemy and reward the player
    /// </summary>
    private void Kill()
    {
        MessageDispatcher.SendMessageData(EventStrings.ENEMY__DESTROYED, this);

        if (DeathEffect)
        {
            Instantiate(
                DeathEffect,
                transform.position,
                Quaternion.identity,
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }

        Destroy(gameObject);
    }
    #endregion
}
