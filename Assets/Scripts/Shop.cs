﻿using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;
using Unavia.Utilities;


public class Shop : MonoBehaviour
{
    #region Variables
    public GameObject ShopButton;

    public List<TurretBlueprint> Turrets;

    [DisableIf("@this.Turrets.Count == 0")]
    [Button("Draw Turret Icons", ButtonSizes.Medium), GUIColor(255/256f, 204/256f, 203/256f)]
    private void DrawTurretButtonsUI()
    {
        DrawTurretButtons();
    }
    #endregion

    private BuildManager buildManager;


    #region Unity Methods
    private void Start()
    {
        buildManager = BuildManager.Instance;

        // Initialize the UI
        DrawTurretButtons();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Draw the shop item buttons
    /// </summary>
    private void DrawTurretButtons()
    {
        // Clear the existing buttons
        gameObject.ClearChildren();

        Turrets.ForEach(blueprint => {
            GameObject newButtonObject = Instantiate(ShopButton, transform);
            ShopButton button = newButtonObject.GetComponent<ShopButton>();
            button.Set(blueprint);
        });
    }
    #endregion
}
