﻿using UnityEngine;
using TMPro;

using com.ootii.Messages;

public class Exit : MonoBehaviour
{
    #region Variables
    [Header("UI")]
    public TextMeshProUGUI LivesText;

    [Header("Effects")]
    public ParticleSystem DamageEffect;
    [Range(1f, 20f)]
    public float MinDamageSpawnRate = 2f;
    [Range(1f, 100f)]
    public float MaxDamageSpawnRate = 10f;

    private ParticleSystem.EmissionModule emissionModule;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register handlers
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.AddListener(EventStrings.PLAYER__LOST_LIVES, OnPlayerLostLives);
    }

    private void Start()
    {
        emissionModule = DamageEffect.emission;

        DamageEffect.Stop();
    }

    private void OnDestroy()
    {
        // Remove handlers
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
        MessageDispatcher.RemoveListener(EventStrings.PLAYER__LOST_LIVES, OnPlayerLostLives);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Once the game is over enemies do not need to be tracked
        if (GameManager.Instance.IsGameOver) return;

        // Enemies reaching the exist should subtract lives
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (enemy == null) return;

        // Notify listeners that an enemy reached the exit
        MessageDispatcher.SendMessageData(EventStrings.ENEMY__REACHED_EXIT, enemy);

        // NOTE: Enemy is automatically destroyed after reaching last waypoint
    }
    #endregion


    #region Handlers
    /// <summary>
    /// Handle "game over" state
    /// </summary>
    /// <param name="message">Game over data</param>
    private void OnGameOver(IMessage message)
    {
        GameOverData gameOverData = (GameOverData)message.Data;

        if (!gameOverData.Won)
            LivesText.text = "-";
    }

    /// <summary>
    /// Update the lives display
    /// </summary>
    /// <param name="message">Player lives information</param>
    private void OnPlayerLostLives(IMessage message)
    {
        PlayerLostLivesData lostLivesData = (PlayerLostLivesData)message.Data;

        if (lostLivesData.LivesLeft > 0)
          LivesText.text = lostLivesData.LivesLeft.ToString();

        // Increase the amount of "damage" particles when losing lives
        if (lostLivesData.LivesLost > 0)
        {
            if (DamageEffect)
            {
                if (DamageEffect.isStopped)
                    DamageEffect.Play();

                int livesRemaining = lostLivesData.LivesStart - lostLivesData.LivesLeft;
                float damageDonePercentage = (float)livesRemaining / lostLivesData.LivesStart;
                float particleSpawnRate = Mathf.Lerp(MinDamageSpawnRate, MaxDamageSpawnRate, damageDonePercentage);

                // DamageEffect.emission.rateOverTime = particleSpawnRate;
                emissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(particleSpawnRate);
            }
        }
    }
    #endregion
}
