﻿using UnityEngine;


[ExecuteAlways]
[RequireComponent(typeof(Waypoints))]
public class WaypointsDebug : MonoBehaviour
{
    #region Variables
    private Waypoints waypoints;
    private DebugManager debug;
    #endregion


    #region Unity Methods
    public void Start()
    {
        debug = DebugManager.Instance;
        waypoints = GetComponent<Waypoints>();
        waypoints.SetPoints();
    }
    #endregion


    #region Debug Methods
    public void OnDrawGizmos()
    {
        if (debug.ShowWaypointLines == GizmoDebug.ALWAYS)
            DrawWaypointLines();

        if (debug.ShowWaypoints == GizmoDebug.ALWAYS)
            DrawWaypointDots();
    }

    public void OnDrawGizmosSelected()
    {
        if (debug.ShowWaypointLines == GizmoDebug.SELECTED)
            DrawWaypointLines();

        if (debug.ShowWaypoints == GizmoDebug.SELECTED)
            DrawWaypointDots();
    }

    private void DrawWaypointDots()
    { 
        if (Waypoints.Points is null || Waypoints.Points.Length == 0)
            waypoints.SetPoints();

        Gizmos.color = Color.red;
        for (int i = 0; i < Waypoints.Points.Length; i++)
        {
            Gizmos.DrawSphere(Waypoints.Points[i].position, 0.1f);
        }
    }

    private void DrawWaypointLines()
    {
        if (Waypoints.Points is null || Waypoints.Points.Length == 0)
            waypoints.SetPoints();

        Gizmos.color = Color.yellow;
        for (int i = 0; i < Waypoints.Points.Length - 1; i++)
        {
            Gizmos.DrawLine(
                Waypoints.Points[i].position,
                Waypoints.Points[i + 1].position
            );
        }
    }
    #endregion
}
