﻿using System;
using UnityEngine;


[Serializable]
public struct TileCoordinate
{
    #region Variables
    public int X;
    public int Y;
    #endregion


    #region Custom Methods
    public TileCoordinate(int x, int y)
    {
        X = x;
        Y = y;
    }

    /// <summary>
    /// Convert a Coordinate to a Vector3
    /// </summary>
    /// <returns></returns>
    public Vector3 ToVector3(float yLevel = 1.0f)
    {
        return new Vector3(X, yLevel, Y);
    }

    /// <summary>
    /// Determine whether two coordinates are unequal
    /// </summary>
    /// <param name="first">First coordinate</param>
    /// <param name="second">Second coordinate</param>
    /// <returns>Whether two coordinates are unequal</returns>
    public static bool operator !=(TileCoordinate first, TileCoordinate second)
    {
        return first.X != second.X || first.Y != second.Y;
    }

    /// <summary>
    /// Determine whether two coordinates are equal
    /// </summary>
    /// <param name="first">First coordinate</param>
    /// <param name="second">Second coordinate</param>
    /// <returns>Whether two coordinates are equal</returns>
    public static bool operator ==(TileCoordinate first, TileCoordinate second)
    {
        return first.X == second.X && first.Y == second.Y;
    }

    /// <summary>
    /// Determine another coordinate is equal
    /// </summary>
    /// <param name="other">Other coordinate</param>
    /// <returns>Whether two coordinates are equal</returns>
    public override bool Equals(object other)
    {
        try
        {
            TileCoordinate otherCoordinate = (TileCoordinate)other;

            return otherCoordinate == this;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Get the hash code from the object
    /// </summary>
    /// <returns>Object hash code</returns>
    public override int GetHashCode()
    {
        var hashCode = 1861411795;
        hashCode = hashCode * -1521134295 + X.GetHashCode();
        hashCode = hashCode * -1521134295 + Y.GetHashCode();
        return hashCode;
    }
    #endregion
}

