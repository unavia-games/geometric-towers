﻿using UnityEngine;
using UnityEngine.EventSystems;

using com.ootii.Messages;
using Unavia.Utilities;

public class OldTile : MonoBehaviour
{
    #region Variables
    [Header("Objects")]
    public GameObject Base = null;

    [Header("Colors")]
    public Color StartColor = Color.white;
    public Color HoverColor = Color.grey;
    public Color BuildColor = Color.blue;
    public Color InvalidColor = Color.red;

    public GameObject Turret { get; private set; }
    public TurretBlueprint TurretBlueprint { get; private set; }

    private Renderer baseRenderer;
    private BuildManager buildManager;
    #endregion


    #region Unity Methods
    private void Start()
    {
        baseRenderer = Base.GetComponent<Renderer>();
        buildManager = BuildManager.Instance;
    }

    private void OnMouseEnter()
    {
        // Avoid clicking through shop menu items
        if (EventSystem.current.IsPointerOverGameObject()) return;

        baseRenderer.material.color = HoverColor;

        // Only highlight squares for building when a turret is selected
        if (buildManager.HasBlueprintSelected)
        {
            if (Turret || !buildManager.CanBuildBlueprint)
            {
                baseRenderer.material.color = InvalidColor;
            }
            else
            {
                baseRenderer.material.color = BuildColor;
            }
        }
    }

    private void OnMouseExit()
    {
        baseRenderer.material.color = StartColor;
    }

    private void OnMouseDown()
    {
        // Avoid clicking through UI items (shop, management menu, etc)
        if (EventSystem.current.IsPointerOverGameObject()) return;

        // Selecting an existing turret should display the management UI
        if (Turret) {
            buildManager.SelectTile(this);
            return;
        }

        // Can only build a turret when a turret is selected (on an empty tile)
        if (buildManager.SelectedBlueprint != null)
        {
            Turret = buildManager.BuildTurret(this);
            if (!Turret) return;

            TurretBlueprint = buildManager.SelectedBlueprint;
            return;
        }

        // Clicking on an empty tile (without turret to build) should clear the management menu
        buildManager.DeselectTile();
    }
    #endregion


    #region
    /// <summary>
    /// Refund the tile's turret
    /// </summary>
    public void RefundTurret()
    {
        if (!Turret || TurretBlueprint == null) return;

        // Update the build manager
        buildManager.DeselectTile();

        // Indicate refund with particle effect
        if (TurretBlueprint.RefundEffect)
        { 
            Instantiate(
                TurretBlueprint.RefundEffect,
                Base.transform.position + Vector3.up * 0.2f,
                Quaternion.identity,
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }

        if (buildManager.TurretRefundSound)
            AudioManager.Instance.PlayEffect(buildManager.TurretRefundSound, transform.position, 0.5f);

        // Notify listeners that a turret has been refunded
        MessageDispatcher.SendMessageData(EventStrings.TURRET__REFUNDED, TurretBlueprint);

        // Clear the refunded turret references
        Destroy(Turret);
        Turret = null;
        TurretBlueprint = null;
    }
    #endregion
}
