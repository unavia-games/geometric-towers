﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector;
using Unavia.Utilities;

namespace Unavia.GeometricTowers
{
    [ExecuteAlways]
    [RequireComponent(typeof(MapManager))]
    public class MapGenerator : ExtendedMonoBehaviour
    {
        #region Variables
        [BoxGroup]
        [Required]
        public MapConfig Config;

        [PropertySpace(SpaceBefore = 8)]
        [Required]
        public Transform TilesParent;
        public GameObject FloorPlane;
        [PropertySpace(SpaceBefore = 0, SpaceAfter = 8)]
        [Range(-5f, 0f)]
        public float FloorHeight = -0.5f;

        [BoxGroup("Actions")]
        [Button("Generate Map", ButtonSizes.Medium), GUIColor(0.3f, 0.6f, 0.6f)]
        public void _GenerateMap()
        {
            // Prompt user to confirm overwriting existing map
            if (TilesParent.childCount > 0)
            {
                bool confirmation = EditorUtility.DisplayDialog(
                  "Regenerate Map?",
                  "Are you sure you want regenerate the map tiles?",
                  "Confirm",
                  "Cancel"
                );

                if (!confirmation) return;
            }

            GenerateMap();
        }

        [PropertySpace(4)]
        [HorizontalGroup("Actions/ClearButtons")]
        [DisableIf("@this.TilesParent.childCount == 0")]
        [Button("Clear Map", ButtonSizes.Medium), GUIColor(1f, 0.8f, 0.8f)]
        public void _ClearMap()
        {
            bool confirmation = EditorUtility.DisplayDialog(
                "Clear Map?",
                "Are you sure you want clear the map tiles?",
                "Confirm",
                "Cancel"
            );

            if (!confirmation) return;

            ClearGameObjects();
        }

        [PropertySpace(4)]
        [HorizontalGroup("Actions/ClearButtons")]
        [DisableIf("@this.TilesParent.childCount == 0")]
        [Button("Reset Map", ButtonSizes.Medium), GUIColor(1f, 0.9f, 0.9f)]
        public void _ResetMap()
        {
            bool confirmation = EditorUtility.DisplayDialog(
                "Reset Map?",
                "Are you sure you want reset the map tiles (positions, etc)?",
                "Confirm",
                "Cancel"
            );

            if (!confirmation) return;

            ResetGameObjects();
        }

        private MapManager mapManager { get { return MapManager.Instance; } }
        private BlockManager blockManager { get { return BlockManager.Instance; } }
        // private TileConfig[,] tileConfigs = new TileConfig[0,0];
        private List<Tile> mapTiles = new List<Tile>();
        #endregion


        #region Unity Methods
        #endregion


        #region Custom Methods
        // 1) Generate map tile configs
        // 2) Create map tiles
        // 3) Position map tiles

        /// <summary>
        /// Create/instantiate the map tiles
        /// </summary>
        /// <remarks>This is done separately from positioning so that tiles can be repositioned without resetting</remarks>
        /// <returns>Instantiated map tiles</returns>
        private List<Tile> CreateTiles()
        {
            // Clear the existing map objects
            ClearGameObjects();
            mapTiles.Clear();

            for (int y = 0; y < Config.MapSize.y; y++)
            {
                for (int x = 0; x < Config.MapSize.x; x++)
                {
                    TileCoordinate coordinates = new TileCoordinate(x, y);
                    Block defaultTileBlock = blockManager.GetBlock("Default");

                    GameObject tileObject = Instantiate(blockManager.TilePrefab, TilesParent);
                    Tile tileComponent = tileObject.AddComponent<Tile>();
                    tileComponent.Init(coordinates, defaultTileBlock, mapManager);

                    mapTiles.Add(tileComponent);
                }
            }

            return mapTiles;
        }

        /// <summary>
        /// Position the map tiles
        /// </summary>
        /// <remarks>This is done separately from creating so that tiles can be repositioned without resetting</remarks>
        private void PositionTiles()
        {
            // Ensure tile parent is properly positioned
            TilesParent.transform.position = Vector3.zero;

            // Center and scale the floor plane accordingly
            if (FloorPlane)
            {
                FloorPlane.SetActive(true);

                float scaleMultiplier = 10f / Config.TileSize;
                Vector3 floorScale = new Vector3(
                    Config.MapSize.x / scaleMultiplier + Config.TileSize / 10,
                    1f,
                    Config.MapSize.y / scaleMultiplier + Config.TileSize / 10
                );

                FloorPlane.transform.position = Vector3.zero + new Vector3(0, FloorHeight, 0);
                FloorPlane.transform.localRotation = Quaternion.identity;
                FloorPlane.transform.localScale = floorScale;
            }

            // Position each of the tiles
            mapTiles.ToList().ForEach(mapTile => {
                mapTile.Position();
            });
        }

        /// <summary>
        /// Generate a map
        /// </summary>
        public void GenerateMap()
        {
            CreateTiles();
            PositionTiles();

            // Update the map manager with the new data
            mapManager.MapTiles = mapTiles;
            mapManager.MapConfig = Config.Copy();
        }

        /// <summary>
        /// Clear a map's game objects
        /// </summary>
        public void ClearGameObjects()
        {
            ClearChildren(TilesParent);

            FloorPlane.SetActive(false);
        }

        /// <summary>
        /// Reposition all map game objects to original positions
        /// </summary>
        public void ResetGameObjects()
        {
            PositionTiles();
        }
    }
    #endregion
}
