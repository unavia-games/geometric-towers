﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Sirenix.OdinInspector;
using Unavia.Utilities;

namespace Unavia.GeometricTowers
{
    [ExecuteAlways]
    public class MapManager : GameSingleton<MapManager>
    {
        #region Variables
        [BoxGroup("Debug")]
        public bool DebugMode = true;

        [ShowInInspector]
        [BoxGroup("Debug")]
        public bool ShowCoordinates {
            get { return showCoordinates; }
            set { ToggleCoordinates(value); }
        }

        [Sirenix.OdinInspector.ReadOnly]
        public MapConfig MapConfig;
        [Sirenix.OdinInspector.ReadOnly]
        public List<Tile> MapTiles;

        private bool showCoordinates = true;
        #endregion


        #region Unity Methods
        private void Start()
        {
            // TODO: Maybe nothing?
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Toggle the tile coordinate displays
        /// </summary>
        /// <param name="value">Whether coordinates should be shown</param>
        private void ToggleCoordinates(bool value)
        {
            showCoordinates = value;

            if (MapTiles != null && MapTiles.Count > 0)
                MapTiles.ForEach(tile => tile.ToggleCoordinates(ShowCoordinates));
        }
        #endregion
    }
}
