﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Unavia.GeometricTowers
{
    public abstract class MapUtils
    {
        /// <summary>
        /// Get the world position for a map coordinate.
        /// </summary>
        /// <returns>World position from map coordinate.</returns>
        /// <param name="x">Map X coordinate.</param>
        /// <param name="y">Map Y coordinate.</param>
        public static Vector3 GetPositionFromCoordinate(MapConfig mapConfig, int x, int y)
        {
            // NOTE: Coordinates are centered around (0,0)
            return GetPositionFromCoordinate(mapConfig, x, y, 0f);
        }

        /// <summary>
        /// Get the world position for a map coordinate.
        /// </summary>
        /// <param name="mapConfig">Map config</param>
        /// <param name="x">Map X coordinate</param>
        /// <param name="y">Map Y coordinate</param>
        /// <param name="height">Height of returned coordinate</param>
        /// <returns>World position from map coordinate.</returns>
        public static Vector3 GetPositionFromCoordinate(MapConfig mapConfig, int x, int y, float height)
        {
            // NOTE: Coordinates are centered around (0,0)
            return new Vector3(
                x * mapConfig.TileSize - (mapConfig.MapSize.x * mapConfig.TileSize / 2f) + (mapConfig.TileSize / 2f),
                height,
                y * mapConfig.TileSize - (mapConfig.MapSize.y * mapConfig.TileSize / 2f) + (mapConfig.TileSize / 2f)
            );
        }

        public static Vector3 GetCoordinateFromPosition(MapConfig mapConfig, Vector3 position, float height)
        {
            return new Vector3(
                (position.x - (mapConfig.TileSize / 2f) + (mapConfig.MapSize.x * mapConfig.TileSize / 2f)) / mapConfig.TileSize,
                height,
                (position.y - (mapConfig.TileSize / 2f) + (mapConfig.MapSize.y * mapConfig.TileSize / 2f)) / mapConfig.TileSize
            );
        }

        public static Vector3 GetCoordinateFromPosition(MapConfig mapConfig, Vector3 position)
        {
            return GetCoordinateFromPosition(mapConfig, position, position.y);
        }
    }
}
