﻿using System;
using UnityEngine;

using Sirenix.OdinInspector;


[Serializable]
public class MapConfig
{
    #region Variables
    /// <summary>
    /// Map size
    /// </summary>
    public Vector2Int MapSize = new Vector2Int(5, 5);
    /// <summary>
    /// Tile size
    /// </summary>
    [Range(1f, 3f)]
    public float TileSize = 1f;
    /// <summary>
    /// Tile percentage occupied by outline
    /// </summary>
    [Range(0f, 0.25f)]
    public float TileOutlinePercent = 0.1f;
    #endregion


    #region Custom Methods
    /// <summary>
    /// Clone the map config
    /// </summary>
    /// <returns>Cloned map configuration</returns>
    public MapConfig Copy()
    {
        MapConfig clone = new MapConfig();
        clone.MapSize = MapSize;
        clone.TileSize = TileSize;
        clone.TileOutlinePercent = TileOutlinePercent;

        return clone;
    }
    #endregion
}
