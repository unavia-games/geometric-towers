﻿using UnityEngine;


public class Waypoints : MonoBehaviour
{
    #region Variables
    public static Transform[] Points;
    #endregion


    #region Unity Methods
    public void Start()
    {
        SetPoints();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Find the waypoints within the path
    /// </summary>
    public void SetPoints()
    {
        Points = new Transform[transform.childCount];
        for (int i = 0; i < Points.Length; i++)
        {
            Points[i] = transform.GetChild(i);
        }
    }
    #endregion
}
