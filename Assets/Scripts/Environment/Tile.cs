﻿using UnityEngine;

using Sirenix.OdinInspector;
using TMPro;
using Unavia.Utilities;


namespace Unavia.GeometricTowers
{
    public class Tile : ExtendedMonoBehaviour
    {
        #region Variables
        /// <summary>
        /// Map tile coordinates
        /// </summary>
        [ShowInInspector]
        [Sirenix.OdinInspector.ReadOnly]
        public TileCoordinate Coordinates { get; private set; }
        /// <summary>
        /// Tile block/floor game object
        /// </summary>
        [Sirenix.OdinInspector.ReadOnly]
        public GameObject BlockGameObject;
        /// <summary>
        /// Tile type
        /// </summary>
        [ShowInInspector]
        [Sirenix.OdinInspector.ReadOnly]
        public Block Type { get; private set; }

        private MapManager mapManager;
        private MapConfig mapConfig {
            get { return mapManager ? mapManager.MapConfig : null; }
        }
        private TextMeshProUGUI coordinatesText;
        #endregion Variables


        #region Unity Methods
        private void Start()
        {
            // TODO: Maybe nothing?
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Initialize the tile
        /// </summary>
        /// <param name="coordinates"></param>
        /// <param name="type"></param>
        public void Init(TileCoordinate coordinates, Block type, MapManager mapManager)
        {
            Coordinates = coordinates;
            Type = type;
            this.mapManager = mapManager;

            // Instantiate the tile block (overriding if necessary)
            DrawBlock(true);
        }


        /// <summary>
        /// Set the block game object
        /// </summary>
        /// <param name="block">Block game object</param>
        public void SetBlockGameObject(GameObject block)
        {
            BlockGameObject = block;
        }

        /// <summary>
        /// Tile type should only be set when spawned by map
        /// </summary>
        /// <param name="type">Block type</param>
        public void SetType(Block type)
        {
            // TODO: Update function to support changing block type (from MapEditor)
            Type = type;
        }


        /// <summary>
        /// Draw the tile's block
        /// </summary>
        /// <param name="overrideExisting">Whether to override existing block</param>
        public void DrawBlock(bool overrideExisting = false)
        {
            // May not always want to override existing game object
            if (!overrideExisting && BlockGameObject) return;

            BlockGameObject = Instantiate(Type.Prefab, transform);

            // Tile borders are faked by scaling tiles down
            BlockGameObject.transform.localScale = Vector3.one * (1 - mapConfig.TileOutlinePercent) * mapConfig.TileSize;

            // Debug tile coordinate indicators (must check unactive children)
            if (!coordinatesText)
                coordinatesText = BlockGameObject.GetComponentInChildren<TextMeshProUGUI>(true);

            ToggleCoordinates(mapManager.ShowCoordinates);
        }

        /// <summary>
        /// Position the tile properly
        /// </summary>
        public void Position()
        {
            Vector3 tilePosition = MapUtils.GetPositionFromCoordinate(mapConfig, Coordinates.X, Coordinates.Y);
            transform.position = tilePosition;
        }

        /// <summary>
        /// Toggle the coordinates display
        /// </summary>
        /// <param name="display">Whether to display coordinates</param>
        public void ToggleCoordinates(bool display)
        {
            if (!coordinatesText) return;

            coordinatesText.text = string.Format("{0},{1}", Coordinates.X, Coordinates.Y);
            coordinatesText.gameObject.SetActive(display);
        }
        #endregion
    }
}
