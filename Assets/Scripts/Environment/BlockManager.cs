﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Sirenix.OdinInspector;
using Unavia.Utilities;


namespace Unavia.GeometricTowers
{
    /// <summary>
    /// Types of blocks
    /// </summary>
    public enum BlockType
    { 
        /// <summary>
        /// Enemy destination tile
        /// </summary>
        DESTINATION,
        /// <summary>
        /// Unavailable tiles for building
        /// </summary>
        OBSTACLE,
        /// <summary>
        /// Available tiles for building
        /// </summary>
        OPEN,
        /// <summary>
        /// Pathing tiles
        /// </summary>
        PATH,
        /// <summary>
        /// Enemy spawn tile
        /// </summary>
        SPAWN
    }


    /// <summary>
    /// Map block types and prefabs
    /// </summary>
    [Serializable]
    public class Block
    {
        /// <summary>
        /// Block name
        /// </summary>
        [Required]
        public string Name;
        /// <summary>
        /// Game object/prefab rendered as the tile
        /// </summary>
        [Required]
        public GameObject Prefab;
        /// <summary>
        /// Block type
        /// </summary>
        [Required]
        public BlockType Type;
    }


    public class BlockManager : GameSingleton<BlockManager>
    {
        #region Variables
        [InfoBox("All tiles share the same parent prefab, but with different 'Block' children.", InfoMessageType = InfoMessageType.Info)]
        public GameObject TilePrefab;

        [PropertySpace]
        public List<Block> Blocks;
        #endregion


        #region Custom Methods
        /// <summary>
        /// Get a block from the block map
        /// </summary>
        /// <param name="name">Block search name</param>
        /// <returns>Matching block</returns>
        public Block GetBlock(string name)
        {
            bool isValidBlock = Blocks.ToList().Any(b => b.Name == name);

            if (!isValidBlock) throw new Exception("No matching block type found for " + name);

            return Blocks.ToList().Find(b => b.Name == name);
        }
        #endregion
    }
}
