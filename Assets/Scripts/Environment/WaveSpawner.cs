﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using com.ootii.Messages;
using Unavia.Utilities;

public class WaveSpawner : MonoBehaviour
{
    #region Variables
    [Header("Config")]
    public Wave[] Waves;
    /// <summary>
    /// Whether to wait until all enemies have died to spawn another wave
    /// </summary>
    public bool WaitBetweenWaves = true;
    [Range(2f, 10f)]
    public float TimeBetweenWaves = 5f;
    [Range(0f, 5f)]
    public float WaveWarningTime = 1f;

    [Header("Objects")]
    public Transform SpawnPoint;

    [Header("UI")]
    public Image WaveCountdownImage;
    public Animator WaveIndicatorAnimator;

    private int enemiesAlive = 0;
    private int waveNumber = 0;
    private Wave waveConfig = null;
    private float countdown = 0f;
    private bool isSpawningWave = false;
    private bool isFinishedSpawningWaves = false;
    private bool hasWarnedAboutWave = false;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        // Register event handlers
        MessageDispatcher.AddListener(EventStrings.ENEMY__DESTROYED, OnEnemyGone);
        MessageDispatcher.AddListener(EventStrings.ENEMY__REACHED_EXIT, OnEnemyGone);
        MessageDispatcher.AddListener(EventStrings.GAME__OVER, OnGameOver);
    }

    private void Start()
    {
        countdown = TimeBetweenWaves;
    }

    private void OnDestroy()
    {
        // Remove event handlers
        MessageDispatcher.RemoveListener(EventStrings.ENEMY__DESTROYED, OnEnemyGone);
        MessageDispatcher.RemoveListener(EventStrings.ENEMY__REACHED_EXIT, OnEnemyGone);
        MessageDispatcher.RemoveListener(EventStrings.GAME__OVER, OnGameOver);
    }

    private void Update()
    {
        // Spawner should only run while the game is not over
        if (GameManager.Instance.IsGameOver) return;

        // The wave countdown only starts after the last wave has finished spawning and all enemies are dead
        if (!isSpawningWave && (WaitBetweenWaves ? enemiesAlive == 0 : true))
        {
            // Send warning about approaching wave
            if (!hasWarnedAboutWave && countdown <= WaveWarningTime)
            {
                if (WaveIndicatorAnimator)
                  WaveIndicatorAnimator.SetBool("Blinking", true);

                MessageDispatcher.SendMessageData(EventStrings.WAVE__APPROACHING, WaveWarningTime);
                hasWarnedAboutWave = true;
            }

            // Spawn wave once countdown reaches zero
            if (countdown <= 0f)
            {
                StartCoroutine(SpawnWave());

                if (WaveIndicatorAnimator)
                  WaveIndicatorAnimator.SetBool("Blinking", false);

                hasWarnedAboutWave = false;
                countdown = TimeBetweenWaves;
            }
            else
            {
                countdown -= Time.deltaTime;

                UpdateCountdownClock();
            }
        }

    }
    #endregion


    #region Handlers
    /// <summary>
    /// Decrease the amount of enemies alive
    /// </summary>
    /// <param name="message">UNUSED</param>
    private void OnEnemyGone(IMessage message)
    {
        enemiesAlive--;

        // The game is over when all enemies are dead and there are no more waves
        if (enemiesAlive == 0 && isFinishedSpawningWaves)
            GameManager.Instance.WinGame();
    }

    private void OnGameOver(IMessage message)
    {
        isSpawningWave = false;

        ToggleCountdownClock(false);
    }
    #endregion


    #region Custom Methods

    /// <summary>
    /// Spawn a new wave
    /// </summary>
    /// <returns>Iterator</returns>
    private IEnumerator SpawnWave()
    {
        ToggleCountdownClock(false);

        waveNumber++;
        waveConfig = Waves[waveNumber - 1];

        // Notify listeners about the wave change
        MessageDispatcher.SendMessageData(EventStrings.WAVE__CHANGE, waveNumber);

        isSpawningWave = true;

        for (int i = 0; i < waveConfig.EnemyCount; i++)
        {
            // Spawner should only run while the game is not over
            if (GameManager.Instance.IsGameOver) break;

            SpawnEnemy(waveConfig.EnemyPrefab);
            yield return new WaitForSeconds(waveConfig.SpawnRate);
        }

        // Prevent spawning waves after reaching the end of the configs
        if (waveNumber == Waves.Length)
            isFinishedSpawningWaves = true;

        isSpawningWave = false;
    }

    /// <summary>
    /// Spawn a new enamy
    /// </summary>
    /// <param name="enemyPrefab">Enemy prefab to spawn</param>
    private void SpawnEnemy(GameObject enemyPrefab)
    {
        Instantiate(enemyPrefab, SpawnPoint.position, SpawnPoint.rotation, TemporaryObjectsManager.Instance.TemporaryChildren);
        enemiesAlive++;
    }

    /// <summary>
    /// Update the wave countdown clock
    /// </summary>
    private void UpdateCountdownClock()
    {
        if (!WaveCountdownImage) return;

        // Ensure clock is enabled
        ToggleCountdownClock(true);

        float timeUntilWave = countdown > Mathf.Epsilon
            ? countdown / TimeBetweenWaves
            : 0;
        WaveCountdownImage.fillAmount = timeUntilWave;
    }

    /// <summary>
    /// Toggle the wave countdown clock
    /// </summary>
    /// <param name="isEnabled">Whether wave countdown clock is enabled</param>
    private void ToggleCountdownClock(bool isEnabled)
    {
        if (!WaveCountdownImage) return;

        // Only update clock when necessary
        if (isEnabled == WaveCountdownImage.enabled) return;

        WaveCountdownImage.enabled = isEnabled;
    }
    #endregion
}
