﻿using UnityEngine;


[ExecuteAlways]
[RequireComponent(typeof(Turret))]
public class TurretDebug : MonoBehaviour
{
    #region Variables
    private Turret turret;
    private DebugManager debug;
    #endregion


    #region Unity Methods
    private void Start()
    {
        debug = DebugManager.Instance;
        turret = GetComponent<Turret>();
    }
    #endregion


    #region Debug Methods
    private void OnDrawGizmos()
    {
        if (debug.ShowTurretRange == GizmoDebug.ALWAYS)
          DrawTurretRange();

        if (debug.ShowTurretAim == GizmoDebug.ALWAYS)
          DrawTurretAim();
    }

    private void OnDrawGizmosSelected()
    {
        if (debug.ShowTurretRange == GizmoDebug.SELECTED)
          DrawTurretRange();

        if (debug.ShowTurretAim == GizmoDebug.SELECTED)
          DrawTurretAim();
    }

    private void DrawTurretAim()
    { 
        Vector3 targetPoint = turret.SpawnPoint.position + turret.SpawnPoint.rotation * Vector3.forward * turret.Range;
        Gizmos.DrawLine(turret.SpawnPoint.position, targetPoint);
    }

    private void DrawTurretRange()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, turret.Range);
    }
    #endregion
}
