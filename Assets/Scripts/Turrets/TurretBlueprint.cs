﻿using UnityEngine;

using Sirenix.OdinInspector;


[CreateAssetMenu(fileName = "New Turret", menuName = "New Turret", order = 51)]
public class TurretBlueprint : ScriptableObject
{
    #region Variables
    [Header("Attributes")]
    public string Name;
    public string Code;
    [Range(0, 1000)]
    public int BaseCost = 100;
    [ReadOnly]
    [ShowInInspector]
    public int Cost { get { return BaseCost; } }
    [Range(0, 500)]
    public int BaseRefund = 50;
    [ReadOnly]
    [ShowInInspector]
    public int Refund { get { return BaseRefund; } }

    [Header("Objects")]
    public GameObject Prefab;

    [Header("Effects")]
    public GameObject BuildEffect;
    public GameObject RefundEffect;

    [Header("UI")]
    public Sprite ShopIcon;
    #endregion
}
