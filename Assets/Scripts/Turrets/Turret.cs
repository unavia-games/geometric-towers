﻿using UnityEngine;

using Unavia.Utilities;

public enum TurretType
{ 
    PROJECTILE,
    PROJECTILE_BURST,
    LASER
}

public class Turret : MonoBehaviour
{
    #region Variables
    [Header("Attributes")]
    public TurretType Type = TurretType.PROJECTILE;
    [Range(0.2f, 5f)]
    public float FireRate = 1f;
    /// <summary>
    /// Maximum angle to target allowed before shooting
    /// </summary>
    [Range(5, 45)]
    public int FireAngle = 10;
    [Range(1, 100)]
    public int ProjectileRate = 25;
    [Range(1f, 10f)]
    public float Range = 2f;
    [Range(10, 250)]
    public int TurnSpeed = 10;
    [Range(1f, 100f)]
    public float Damage = 10f;
    [Range(1, 20)]
    public int ChecksPerSecond = 4;

    [Header("Laser")]
    public ParticleSystem LaserImpactEffect;
    [Range(0f, 5f)]
    public float LaserDamage = 0f;
    [Range(0f, 1f)]
    public float LaserSlowdownPercentage = 0f;

    [Header("Objects")]
    public Transform BulletPrefab;
    public Transform SpawnPoint;
    public Transform RotationTransform;
    public Light LaserLight;

    [Header("Effects")]
    public AudioClip TurretShootSound;

    [Header("Miscellaneous")]
    public LayerMask EnemyMask;
    [Range(-1f, 1f)]
    public float BuildOffset = 0f;

    private Transform target;
    private LineRenderer laserRenderer;
    private float fireCountdown;
    #endregion

    #region Unity Methods
    private void Start()
    {
        // Only check for a valid target 4 times a second
        InvokeRepeating("CheckTarget", 0f, 1f / ChecksPerSecond);

        if (Type == TurretType.LASER)
        {
            laserRenderer = GetComponent<LineRenderer>();
            if (LaserLight)
              LaserLight.enabled = false;
            if (LaserImpactEffect)
              LaserImpactEffect.Stop();
        }
    }

    private void Update()
    {
        // Turrets cannot operate when the game is over
        if (GameManager.Instance.IsGameOver)
        {
            Laser(false);
            return;
        }

        LockOnTarget();

        // TODO: Fix this pile of garbage...really, Brackeys?...
        if (target)
        {
            Vector3 targetDirection = (target.position - transform.position).normalized;
            Vector3 currentDirection = RotationTransform.forward;
            float angle = Vector3.Angle(targetDirection, currentDirection);

            if (Type == TurretType.LASER)
            {
                // Need to pass condition to function to properly disable line renderer
                Laser(angle <= FireAngle);
            }
            else if (fireCountdown <= Mathf.Epsilon)
            {
                // Only permit shooting when within a certain angle of target
                if (angle > FireAngle) return;

                Shoot();
                fireCountdown = 1f / FireRate;
            }
        }
        else if (Type == TurretType.LASER)
        {
            Laser(false);
        }

        fireCountdown -= Time.deltaTime;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Lock on a target (rotate towards)
    /// </summary>
    private void LockOnTarget()
    { 
        // Rotate the turret to face its target
        if (target && RotationTransform)
        {
            Vector3 direction = target.position - transform.position;
            RotationTransform.rotation = RotateTowards(direction, 1f);
        }
    }

    /// <summary>
    /// Find the rotations for a target direction
    /// </summary>
    /// <param name="direction">Target direction</param>
    /// <param name="turnMultiplier">Turn speed multiplier</param>
    /// <returns>Rotation for a target direction</returns>
    private Quaternion RotateTowards(Vector3 direction, float turnMultiplier)
    {
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(
            RotationTransform.rotation,
            lookRotation,
            Time.deltaTime * TurnSpeed / turnMultiplier
        ).eulerAngles;

        return Quaternion.Euler(0f, rotation.y, 0f);
    }

    /// <summary>
    /// Determine whether a valid target is selected
    /// </summary>
    private void CheckTarget()
    {
        if (target && Vector3.Distance(transform.position, target.position) > Range)
            target = null;

        // Only search for a new target when the old one does not exist
        if (!target)
            UpdateTarget();
    }

    /// <summary>
    /// Check for a valid target
    /// </summary>
    private void UpdateTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, Range, EnemyMask);

        float closestDistance = Mathf.Infinity;
        for (int i = 0; i < colliders.Length; i++)
        {
            float distanceToCollider = Vector3.Distance(transform.position, colliders[i].transform.position);
            if (distanceToCollider <= Range && distanceToCollider < closestDistance)
            {
                closestDistance = distanceToCollider;
                target = colliders[i].transform;
            }
        }
    }

    /// <summary>
    /// Shoot at the targeted enemy
    /// </summary>
    private void Shoot()
    {
        Transform projectileObj = Instantiate(
            BulletPrefab,
            SpawnPoint.position,
            SpawnPoint.rotation,
            TemporaryObjectsManager.Instance.TemporaryChildren
        );

        if (TurretShootSound)
            AudioManager.Instance.PlayEffect(TurretShootSound, transform.position, 0.5f);

        Projectile projectile = projectileObj.GetComponent<Projectile>();
        projectile.Speed = ProjectileRate;
        projectile.Damage = Damage;
        projectile.SetTarget(target);
    }

    private void Laser(bool canLaser)
    {
        // Apply damage from laser
        if (target)
        {
            Enemy enemy = target.GetComponent<Enemy>();
            enemy.TakeDamage(LaserDamage * Time.deltaTime);
            enemy.SlowDown(LaserSlowdownPercentage);
        }

        if (!laserRenderer) return;

        // Only toggle whether component is enabled if necessary
        if (canLaser && !laserRenderer.enabled)
        { 
            laserRenderer.enabled = true;
            LaserImpactEffect.Play();
            LaserLight.enabled = true;
        }
        else if (!canLaser && laserRenderer.enabled)
        {
            laserRenderer.enabled = false;
            LaserImpactEffect.Stop();
            LaserLight.enabled = false;
        }

        // Avoid updating line renderer and effect while disabled
        if (!canLaser) return;

        laserRenderer.SetPosition(0, SpawnPoint.position);
        laserRenderer.SetPosition(1, target.position);

        // Position the laser hit effect properly
        Vector3 directionToTurret = SpawnPoint.position - target.position;
        LaserImpactEffect.transform.position = target.position + directionToTurret.normalized * 0.1f;
        LaserImpactEffect.transform.rotation = Quaternion.LookRotation(directionToTurret);
    }
    #endregion
}
