﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unavia.Utilities;

public class Projectile : ExtendedMonoBehaviour
{
    #region Variables
    [Header("Attributes")]
    [Range(1f, 20)]
    public float Speed = 5f;
    [Range(1f, 10f)]
    public float Damage = 1f;
    [Range(0f, 5f)]
    public float ExplosionRadius = 0f;
    public GameObject ImpactEffect;
    [SerializeField]
    private LayerMask CollisionMask = new LayerMask();

    private Transform target;
    private const float MAX_PROJECTILE_DISTANCE = 100f;
    private const float MAX_PROJECTILE_LIFETIME = 10f;
    private Vector3 origin;
    #endregion

    #region Unity Methods
    private void Start()
    {
        origin = transform.position;

        // Projectiles have a limited lifetime
        Wait(MAX_PROJECTILE_LIFETIME, () =>
        {
            Destroy(gameObject);
        });
    }


    void Update()
    {
        // Projectiles have a limited range (semi-unusual)
        float distanceTravelled = Vector3.Distance(origin, transform.position);
        if (distanceTravelled > MAX_PROJECTILE_DISTANCE)
        {
            Destroy(gameObject);
            return;
        }

        // Projectile movement requires several steps, first checking for possible
        //   collisions and then either moving the entire frame distance or just
        //   the distance to the collision.
        float frameMoveDistance = Speed * Time.deltaTime;
        if (CheckCollisions(frameMoveDistance, out float distanceToCollision))
        {
            transform.Translate(Vector3.forward * distanceToCollision);
        }
        else
        {
            // Move projectile towards the target ("seeking" approach").
            // When no target is present, projectile should move forward with
            //   last known direction, allowing it to collide with other items.
            if (target)
            {
                Vector3 directionToTarget = target.position - transform.position;
                transform.Translate(directionToTarget.normalized * frameMoveDistance, Space.World);
                transform.LookAt(target);
            }
            else
            { 
              transform.Translate(Vector3.forward * frameMoveDistance);
            }
        }
    }

    private void OnCollisionEnter(Collision collider)
    {
        OnCollision(collider.gameObject, collider.GetContact(0).point);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Check whether a collision will occur within the frame (prevents collision clipping)
    /// </summary>
    /// <remarks>This may not handle bullet target "seeking" (see "CheckDistance").</remarks>
    /// <param name="frameDistance">Distance that will be moved in frame</param>
    public bool CheckCollisions(float frameDistance, out float hitDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, frameDistance))
        {
            hitDistance = hit.distance;
            return true;
        }

        hitDistance = 0;
        return false;
    }

    public bool CheckDistance(float frameDistance, out float hitDistance)
    {
        Vector3 directionToTarget = target.position - transform.position;
        if (directionToTarget.magnitude <= frameDistance)
        {
            hitDistance = directionToTarget.magnitude;
            return true;
        }

        hitDistance = 0;
        return false;
    }

    /// <summary>
    /// Handle bullet collision with other entities
    /// </summary>
    /// <param name="collider">Colliding game object</param>
    /// <param name="hitPoint">Location of collision</param>
    private void OnCollision(GameObject collider, Vector3 hitPoint)
    {
        // Only detect collisions on appropriate layers
        if (!CollisionMask.ContainsLayer(collider.layer)) return;

        // Apply damage to collider if appropriate
        /* Damageable damageableObject = collider.GetComponent<Damageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeDamage(Damage, gameObject);
        } */

        // Spawn bullet impact (if provided)
        if (ImpactEffect) {
            Instantiate(
                ImpactEffect,
                hitPoint,
                transform.rotation,
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }

        // Handle normal or exploding projectiles
        if (ExplosionRadius > 0f)
        {
            // Collision mask may also include some environment items
            Collider[] colliders = Physics.OverlapSphere(hitPoint, ExplosionRadius, CollisionMask);
            foreach (Collider entity in colliders)
            {
                Enemy enemy = entity.gameObject.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.TakeDamage(Damage);
                }
            }
        }
        else
        {
            Enemy enemy = collider.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(Damage);
            }
        }

        Destroy(gameObject);
    }

    /// <summary>
    /// Set the projectile target
    /// </summary>
    /// <remarks>Projectile targeting is used to avoid projectiles missing due to distance travelled.</remarks>
    /// <param name="target"></param>
    public void SetTarget(Transform target)
    {
        this.target = target;
    }
    #endregion
}
